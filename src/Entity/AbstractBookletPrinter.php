<?php

namespace App\Entity;

abstract class AbstractBookletPrinter
{
    protected abstract function getName(): string;

    protected abstract function getPageCount(): int;

    protected abstract function printFrontCover(): string;

    protected abstract function printTableOfContents(): string;

    protected abstract function printPage(int $pageNumber): string;

    protected abstract function printIndex(): string;

    protected abstract function printBackCover(): string;

    // This is the 'template method'
    public final function printBooklet(): string
    {
        echo $this->printFrontCover() . '<br/>';
        echo $this->printTableOfContents() . '<br/>';
        for ($i = 1; $i <= $this->getPageCount(); $i++):
            echo $this->printPage($i) . '<br/>';
        endfor;
        echo $this->printIndex() . '<br/>';
        echo $this->printBackCover() . '<br/>';

        return sprintf('%s printed', $this->getName());
    }
}