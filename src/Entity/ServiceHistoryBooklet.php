<?php

namespace App\Entity;

class ServiceHistoryBooklet extends AbstractBookletPrinter
{
    protected function getName(): string {
        return 'ServiceHistoryBooklet';
    }

    protected function getPageCount(): int
    {
        return 12;
    }

    protected function printFrontCover(): string
    {
        return 'Printing front cover for service history booklet';
    }

    protected function printTableOfContents(): string
    {
        return 'Printing table of contents for service history booklet';
    }

    protected function printPage(int $pageNumber): string
    {
        return sprintf('Printing page %d for service history booklet', $pageNumber);
    }

    protected function printIndex(): string
    {
        return 'Printing index for service history booklet';
    }

    protected function printBackCover(): string
    {
        return 'Printing back cover for service history booklet';
    }
}