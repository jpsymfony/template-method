<?php

namespace App\Entity;

class SaloonBooklet extends AbstractBookletPrinter
{
    protected function getName(): string {
        return 'SaloonBooklet';
    }

    protected function getPageCount(): int
    {
        return 12;
    }

    protected function printFrontCover(): string
    {
        return 'Printing front cover for Saloon car booklet';
    }

    protected function printTableOfContents(): string
    {
        return 'Printing table of contents for Saloon car booklet';
    }

    protected function printPage(int $pageNumber): string
    {
        return sprintf('Printing page %d for Saloon car booklet', $pageNumber);
    }

    protected function printIndex(): string
    {
        return 'Printing index for Saloon car booklet';
    }

    protected function printBackCover(): string
    {
        return 'Printing back cover for Saloon car booklet';
    }
}