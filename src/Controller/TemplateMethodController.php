<?php

namespace App\Controller;

use App\Entity\SaloonBooklet;
use App\Entity\ServiceHistoryBooklet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TemplateMethodController extends AbstractController
{
    /**
     * @Route("/template-method", name="template_method")
     */
    public function index()
    {
        return $this->render('template_method/index.html.twig', [
            'saloonBooklet' => new SaloonBooklet(),
            'serviceHistoryBooklet' => new ServiceHistoryBooklet()
        ]);
    }
}
